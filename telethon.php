<?php
declare (strict_types=1);

// Création de la constante pour stocker les 0.10 centimes des sponsors
const DON_PAR_MINUTE = 0.1;
// define("DON", 0.1); A bannir !!!!!

// $listeParticipant stock les données de $infoParticipants
(array) $listeParticipants = [
];

// ******************************************************


// *****************************************************

// Variables
(bool) $courseActive = true;
(int) $contributionTotale = 0.0;
(int) $tempsTotalHeure = 0;

// Creation des constantes pour le calcul des sponsor
(array) $sponsor =[
    "super U" => 0.15,
    "Garage Moulin" => 0.045,
    "Bijouterie Excelior" => 0.045,
    "Boucherie Sanzos" => 0.025,
    "Carrefour Market" => 0.075,
    "Boulangerie Giraud" => 0.02,
    "Cabinet comptable AGC" => 0.045,
    "Lambert et Colin avocats associés" => 0.055,
    "Chez Romain (épicerie)" => 0.05,
    "Le café de la gare (bar-restaurant)" => 0.05,
    "Cinéma Le Plaza" => 0.04,
    "Lions Club" => 0.125,
    "Relai H" => 0.055,
    "Mc Donald's (SARL Bernard)" => 0.095,
    "Starbuck café" => 0.08,
    "La page blanche (libraire-papeterie)" => 0.045
];


print(PHP_EOL . '*******************************************************');

// Boucles fin de course ______________________________________________________________
while ($courseActive):

    print(PHP_EOL . 'Veuillez rentrez les données du participant :' . PHP_EOL);
    // Tableau de stockage avec clé crée pour récupérer les info saisie
    (array) $infoParticipant = [
        "prenom" => "",
        "nom" => "",
        "age" => 0,
        "heure" => 0,
        "minute" => 0,
        "seconde" => 0,
        "tempsMinute" => 0.0,
        "contribution" => 0.0,
        "trancheAge" => ""
    ];

    // Variables
    (string) $saisiePrenom="";
    (string) $saisieNom="";
    (int) $saisieAge= 0;
    (int) $saisieHeure = -1;
    (int) $saisieMinute= -1;
    (int) $saisieSeconde= -1;
    (string) $choixFin = '';

    //demander et stocker le prénom
    while(!$saisiePrenom):
        print("Prénom du participant ? ");
        $saisiePrenom = trim(strval(fgets(STDIN)));
    endwhile;

    //demander et stocker le nom
    while(!$saisieNom):
        print("Nom du participant ? ");
        $saisieNom= trim(strval(fgets(STDIN)));
        endwhile;

    //demander et stocker l'age
    // l'age ne doit pas depasser 125 ans
    while(!($saisieAge > 0 && $saisieAge <= 125)):
        print("Age du participant ? ");
        $saisieAge = intval(trim(strval(fgets(STDIN))));
    endwhile;
    
    // entre 0 et 17 ans seule L'initial du nom est afficher
    // Création des initial pour les moins de 18ans
    if($saisieAge <= 17){
        $saisiePrenom = strtoupper($saisiePrenom[0]);
        $saisieNom = strtoupper($saisieNom[0]);
        Print('Attention âge inférieur à 18 ans ! (Le nom et le prénom ne seront pas afficher entierement)' . PHP_EOL);
    }

    // Renvoie des saisie dans le tableau de stockage
    $infoParticipant["prenom"] = $saisiePrenom;
    $infoParticipant["nom"] = $saisieNom;
    $infoParticipant["age"] = $saisieAge;

    // on verifie que la durée de la course n'est pas 00:00:00
    while ($infoParticipant["heure"] == 0 && $infoParticipant["minute"] == 0 && $infoParticipant["seconde"] == 0):
        
        // Saisie et Stockage des données temps
        while ($saisieHeure < 0): 
            print('Saisir heure(s) ? ');
            $saisieHeure =  intval(trim(strval(fgets(STDIN))));
        endwhile;

        // Pour stocker la valeur de la variable $saisieHeure dans le tableau $infoParticipant["heure"]
        $infoParticipant["heure"]= $saisieHeure;

        while (!($saisieMinute >=0 && $saisieMinute <= 59)):
            print('Saisir minute(s) ? ');
            $saisieMinute =  intval(trim(strval(fgets(STDIN))));
        endwhile;

        // Pour stocker la valeur de la variable $saisieMinute dans le tableau $infoParticipant["minute"]
        $infoParticipant["minute"]= $saisieMinute;

        
        while (!($saisieSeconde >=0 && $saisieSeconde <= 59)):
            print('Saisir seconde(s) ? ');
            $saisieSeconde =  intval(trim(strval(fgets(STDIN))));
        endwhile;

        // Pour stocker la valeur de la variable $saisieSeconde dans le tableau $infoParticipant["seconde"]
        $infoParticipant["seconde"]= $saisieSeconde;
    endwhile;
    
    // Conversion heure et seconde en minute + envoie au tableau $infoParticipant["tempsMinute"]
    $infoParticipant["tempsMinute"] = $infoParticipant["heure"]*60 + $infoParticipant["minute"] + $infoParticipant["seconde"]/60;
    
    // printf('Temps total : %g' . PHP_EOL , $infoParticipant["tempsMinute"]);

    // Calcul de la contribution par temps
    $infoParticipant["contribution"] = $infoParticipant["tempsMinute"] * DON_PAR_MINUTE;

    // Change le print suivant l'age rentrer
    if($saisieAge < 18){
    printf(PHP_EOL . "Le participant %s.%s contribue de %g €, pour un temps donnée de %d heure(s), %d minute(s) et %d seconde(s) de course.". PHP_EOL , 
        $infoParticipant["nom"], 
        $infoParticipant["prenom"], 
        $infoParticipant["contribution"], 
        $infoParticipant["heure"],
        $infoParticipant["minute"],
        $infoParticipant["seconde"]
    );
    } else {
    printf(PHP_EOL . "Le participant %s %s contribue de %g €, pour un temps donnée de %d heure(s), %d minute(s) et %d seconde(s) de course.". PHP_EOL , 
        $infoParticipant["nom"], 
        $infoParticipant["prenom"], 
        $infoParticipant["contribution"], 
        $infoParticipant["heure"],
        $infoParticipant["minute"],
        $infoParticipant["seconde"]
    );
    }   

    //il faut une liste classée par tranche d'age et par le temps décroissant
    // switch tranche d'age
    switch ($infoParticipant["age"]) :
        case ($infoParticipant["age"] < 8) : //equivalent à case ($infoParticipant["age"] == ($infoParticipant["age"] < 8))
            print('Participant dans la tranche d\'âge 8 ans ou moins.' . PHP_EOL);
            $infoParticipant["trancheAge"] = 'Participant dans la tranche d\'âge 8 ans ou moins.';
            break;
        case ($infoParticipant["age"] < 10) :
            print('Participant dans la tranche d\'âge 8 - 10 ans.' . PHP_EOL);
            $infoParticipant["trancheAge"] = 'Participant dans la tranche d\'âge 8 - 10 ans.';
            break;
        case ($infoParticipant["age"] < 14) :
            print('Participant dans la tranche d\'âge 10 - 14 ans.' . PHP_EOL);
            $infoParticipant["trancheAge"] = 'Participant dans la tranche d\'âge 10 - 14 ans.';
            break;
        case ($infoParticipant["age"] < 18) :
            print('Participant dans la tranche d\'âge 14 - 18 ans.' . PHP_EOL);
            $infoParticipant["trancheAge"] = 'Participant dans la tranche d\'âge 14 - 18 ans.';
            break;
        case ($infoParticipant["age"] < 25) :
            print('Participant dans la tranche d\'âge 18 - 25 ans.' . PHP_EOL);
            $infoParticipant["trancheAge"] = 'Participant dans la tranche d\'âge 18 - 25 ans.';
            break;
        case ($infoParticipant["age"] < 45) :
            print('Participant dans la tranche d\'âge 25 - 45 ans.' . PHP_EOL);
            $infoParticipant["trancheAge"] = 'Participant dans la tranche d\'âge 25 - 45 ans.';
            break;
        case ($infoParticipant["age"] < 60) :
            print('Participant dans la tranche d\'âge 45 - 60 ans.' . PHP_EOL);
            $infoParticipant["trancheAge"] = 'Participant dans la tranche d\'âge 45 - 60 ans.';
            break;
        case ($infoParticipant["age"] < 70) :
            print('Participant dans la tranche d\'âge 60 - 70 ans.' . PHP_EOL);
            $infoParticipant["trancheAge"] = 'Participant dans la tranche d\'âge 60 - 70 ans.';
            break;
        default : 
            print('Participant dans la tranche d\'âge 70 ans et plus.' . PHP_EOL);
            $infoParticipant["trancheAge"] = 'Participant dans la tranche d\'âge 70 ans et plus.';
    endswitch;

    // Addition pour le temps total afficher en fin de course
    $contributionTotale = $contributionTotale + $infoParticipant["contribution"];
    printf(PHP_EOL . "La contribution totale de l'événement est de %g €" . PHP_EOL, $contributionTotale);

    $tempsTotalHeure = $tempsTotalHeure + $infoParticipant["tempsMinute"] / 60;

    // Stock les données saisies par l'utilisateur dans le tableau $ListeParticipants[]
    $listeParticipants[]= $infoParticipant;

    // Print et recupère la reponse saisie 
    while ( $choixFin != 'non' && $choixFin != 'oui'):
        print(PHP_EOL . 'Avez vous d\'autres participants a saisir (oui / non) ?' . PHP_EOL);
        $choixFin = trim(strval(fgets(STDIN)));
    endwhile;

    // Défini sur true si "oui" est saisie
    $courseActive = ($choixFin == 'oui');


    // classement/affichage temps de course par participant par ordre décroissant
    // Creation d'un tableau de colonne pour les valeur age pour pouvoir les trier plus tard
    (array) $listeAgeParticipants = array_column($listeParticipants, 'age');
    (array) $listeTempsParticipants = array_column($listeParticipants, 'tempsMinute');

    // Triage du tableau de colonne "age" par ordre ascendant 
    array_multisort($listeAgeParticipants, SORT_ASC, $listeTempsParticipants, SORT_DESC, $listeParticipants);


    // array_multisort($listeTempsParticipants, SORT_DESC, $listeParticipants);


    // Lecture complète du tableau grace à la boucle foreach
    foreach($listeParticipants as $infoParticipant){
        if($saisieAge < 18){
            print(PHP_EOL . '-------------------------------------------------------' . PHP_EOL);
            printf(PHP_EOL . 'Récapitulatif des données saisies pour %s.%s (%d ans)'. PHP_EOL . "%s" . PHP_EOL,
                $infoParticipant["prenom"],
                $infoParticipant["nom"],
                $infoParticipant["age"],
                $infoParticipant["trancheAge"]
        );
        } else {
            print(PHP_EOL . '-------------------------------------------------------' . PHP_EOL);
            printf(PHP_EOL . 'Récapitulatif des données saisies pour %s %s (%d ans)'. PHP_EOL . "%s" . PHP_EOL,
                $infoParticipant["prenom"],
                $infoParticipant["nom"],
                $infoParticipant["age"],
                $infoParticipant["trancheAge"]
        );
        }
        printf("Temps de course de %d heure(s), %d minute(s) et %s seconde(s) (Contribution de %g €)" . PHP_EOL,
            $infoParticipant["heure"],
            $infoParticipant["minute"],
            $infoParticipant["seconde"],
            $infoParticipant["contribution"]
        );

    }

    if($courseActive == "oui"){
        print(PHP_EOL . '_______________________________________________________' . PHP_EOL);
    } else {
        print(PHP_EOL . '-------------------------------------------------------' . PHP_EOL);
    }

    // Exporte et calcul les valeurs du tableau $sponsor[] et les fusionne avec c'est propre clé
endwhile;
foreach($sponsor as $sponsorVar){
    $sponsorVar = $sponsorVar * $contributionTotale;
    print PHP_EOL;
    // Print que les clés du tableau
    print key($sponsor);
    print " ";
    // Print les valeurs calculer 
    print($sponsorVar);
    // Avance le pointeur interne du tableau d'un élément
    next($sponsor);
    print " €" . PHP_EOL;
}


print(PHP_EOL . '-------------------------------------------------------' . PHP_EOL);

printf(PHP_EOL . 'La contribution totale de l\'événement est de %g €, pour un temps total de %d heures.' . PHP_EOL, $contributionTotale, $tempsTotalHeure);

